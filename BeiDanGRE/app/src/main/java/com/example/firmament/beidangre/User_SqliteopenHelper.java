package com.example.firmament.beidangre;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Firmament on 14-11-9.
 */

public class User_SqliteopenHelper extends SQLiteOpenHelper {

    final static private int version = 1;

    final private static SQLiteDatabase.CursorFactory factory = null;

    private static Context context = null;

    private static int num=1;

    private volatile static User_SqliteopenHelper instance = null;

    public static User_SqliteopenHelper getInstance(Context context,int num) {

        if (instance == null||User_SqliteopenHelper.num!= num) {
            synchronized (W_SqliteopenHelper.class) {
                if(instance == null||User_SqliteopenHelper.num!= num) {
                    instance = new User_SqliteopenHelper(context,num);
                }
            }
        }
        return instance;
    }

    private User_SqliteopenHelper (Context context,int num){

        super(context,"Userlib"+num+".db",factory,version);
        User_SqliteopenHelper.num = num;
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase database){
        database.execSQL("CREATE TABLE IF NOT EXISTS  dic ( _id integer primary key autoincrement,dic_id integer,level integer);");

    }

    @Override
    public void onUpgrade(SQLiteDatabase database,int oldVersion,int newVesion){
        ;
    }
}
