package com.example.firmament.beidangre;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ViewFlipper;

/**
 * Created by Firmament on 14-10-30.
 */
public class NoteBookFragment extends Fragment {

    private FragmentManager fragmentManager;

    public ViewFlipper viewFlipper;

    private TextView notebookname1;
    private ProgressBar processing1;
    private TextView wordsnumber1;
    private Button enter1;

    private TextView notebookname2;
    private ProgressBar processing2;
    private TextView wordsnumber2;
    private Button enter2;

    private TextView notebookname3;
    private ProgressBar processing3;
    private TextView wordsnumber3;
    private Button enter3;

    private Button newNoteBook;

    private TextView showNum;

    private NoteBookWordsFragment noteBookWordsFragment;

    private int position = 0;

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {
        final View Layout = inflater.inflate(R.layout.note_book_fragment, container, false);

        viewFlipper = (ViewFlipper)Layout.findViewById(R.id.note_book_Flipper);
        MainMenu.resideMenu.addIgnoredView(viewFlipper);

        showNum = (TextView)Layout.findViewById(R.id.show_position);

        if (All_Lists.NumOfNoteBook == 0){
            viewFlipper.setVisibility(View.INVISIBLE);
            showNum.setVisibility(View.INVISIBLE);
        }

        fragmentManager = getFragmentManager();

        notebookname1 = (TextView)Layout.findViewById(R.id.note_book_name1);
        processing1 = (ProgressBar)Layout.findViewById(R.id.note_book_progress1);
        wordsnumber1 = (TextView)Layout.findViewById(R.id.note_book_numbers1);
        enter1 = (Button)Layout.findViewById(R.id.note_book_enter1);

        notebookname2 = (TextView)Layout.findViewById(R.id.note_book_name2);
        processing2 = (ProgressBar)Layout.findViewById(R.id.note_book_progress2);
        wordsnumber2 = (TextView)Layout.findViewById(R.id.note_book_numbers2);
        enter2 = (Button)Layout.findViewById(R.id.note_book_enter2);

        notebookname3 = (TextView)Layout.findViewById(R.id.note_book_name3);
        processing3 = (ProgressBar)Layout.findViewById(R.id.note_book_progress3);
        wordsnumber3 = (TextView)Layout.findViewById(R.id.note_book_numbers3);
        enter3 = (Button)Layout.findViewById(R.id.note_book_enter3);

        ShowNoteBookInformation(position,viewFlipper.getDisplayedChild());

        newNoteBook = (Button)Layout.findViewById(R.id.new_note_book_button);
        newNoteBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
                View dialogView = layoutInflater.inflate(R.layout.new_notebook_dialog,null);
                final EditText enter_norebookname = (EditText)dialogView.findViewById(R.id.new_note_book_edittext);

                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                AlertDialog dialog = builder.create();
                builder.setIcon(R.drawable.ic_launcher);
                builder.setTitle("给你的单词本起个名字吧");
                builder.setView(dialogView);

                builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.setPositiveButton("确定",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String str = enter_norebookname.getText().toString();
                        All_Lists.CreateList(str);

                        viewFlipper.setVisibility(View.VISIBLE);
                        showNum.setVisibility(View.VISIBLE);

                        position = All_Lists.NumOfNoteBook - 1;
                        ShowNoteBookInformation(position,viewFlipper.getDisplayedChild());

                        SharedPreferences.Editor editor = WelcomePage.sharedPreferences.edit();
                        editor.putBoolean("ifhave_notebook",true);
                        editor.commit();
                    }
                });
                builder.show();
            }
        });

        viewFlipper.setOnTouchListener(new View.OnTouchListener() {
            float StartX;
            float X;

            int locate;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()){
                    case MotionEvent.ACTION_DOWN:
                        StartX = event.getX();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        break;
                    case MotionEvent.ACTION_UP:
                        X = event.getX();
                        if (StartX - X < -100){
                            if (position > 0) {
                                position --;
                                viewFlipper.setInAnimation(getActivity(), R.anim.in_left_right);
                                viewFlipper.setOutAnimation(getActivity(), R.anim.out_left_right);
                                viewFlipper.showPrevious();
                                ShowNoteBookInformation(position,viewFlipper.getDisplayedChild());
                            }
                        }else if (StartX - X > 100){
                            if (position < All_Lists.NumOfNoteBook - 1) {
                                position ++;
                                viewFlipper.setInAnimation(getActivity(), R.anim.in_right_left);
                                viewFlipper.setOutAnimation(getActivity(), R.anim.out_right_left);
                                viewFlipper.showNext();
                                ShowNoteBookInformation(position,viewFlipper.getDisplayedChild());
                            }
                        }else {
                            ;
                        }
                        break;
                    default:
                        break;
                }
                return true;
            }
        });

        enter1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt("ListNo",position);

                noteBookWordsFragment = NoteBookWordsFragment.getInstance(bundle);
                fragmentManager.beginTransaction().addToBackStack(null).replace(R.id.fragment_container0,noteBookWordsFragment).commit();
            }
        });

        enter2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt("ListNo",position);

                noteBookWordsFragment = NoteBookWordsFragment.getInstance(bundle);
                fragmentManager.beginTransaction().addToBackStack(null).replace(R.id.fragment_container0,noteBookWordsFragment).commit();
            }
        });

        enter3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt("ListNo",position);

                noteBookWordsFragment = NoteBookWordsFragment.getInstance(bundle);
                fragmentManager.beginTransaction().addToBackStack(null).replace(R.id.fragment_container0,noteBookWordsFragment).commit();
            }
        });

        return Layout;
    }

    private void ShowNoteBookInformation(int No,int point){
        if (All_Lists.NumOfNoteBook != 0 && No < All_Lists.NumOfNoteBook) {
            if (point == 0) {
                notebookname1.setText(All_Lists.NoteBooks[No].Name);
                processing1.setProgress(All_Lists.NoteBooks[No].precessing);
                wordsnumber1.setText(All_Lists.NoteBooks[No].precessing + "/" + All_Lists.NoteBooks[No].Numbers.toString());
            }else if (point == 1) {
                notebookname2.setText(All_Lists.NoteBooks[No].Name);
                processing2.setProgress(All_Lists.NoteBooks[No].precessing);
                wordsnumber2.setText(All_Lists.NoteBooks[No].precessing + "/" + All_Lists.NoteBooks[No].Numbers.toString());
            }else {
                notebookname3.setText(All_Lists.NoteBooks[No].Name);
                processing3.setProgress(All_Lists.NoteBooks[No].precessing);
                wordsnumber3.setText(All_Lists.NoteBooks[No].precessing + "/" + All_Lists.NoteBooks[No].Numbers.toString());
            }
            showNum.setText((position + 1) + "/" + All_Lists.NumOfNoteBook);
        }
    }

    public static NoteBookFragment getInstance(Bundle bundle){
        NoteBookFragment noteBookFragment = new NoteBookFragment();
        noteBookFragment.setArguments(bundle);
        return noteBookFragment;
    }
}
