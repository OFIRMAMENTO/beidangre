package com.example.firmament.beidangre;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

/**
 * Created by Firmament on 14-11-4.
 */

public class DbManager {

    final private String tablename = "vocabulary";

    final private static String ini= "Is_initialize_db";

    private String DB_PATH = "/data/data/"+"com.example.firmament.berdangre"+"/databases/";

    final private static String name = "gre3000.db";

    private Context context;

    private W_SqliteopenHelper sqliteopenHelper;

    private SQLiteDatabase sqLiteDatabase;

    private User_SqliteopenHelper user_sqliteopenHelper;

    private SQLiteDatabase user_sqlitedatabase;

    private int level_default = 0 ;

    public DbManager(Context context){
        this.context = context;
        SharedPreferences myPrefs = context.getSharedPreferences("user", Context.MODE_PRIVATE);
        boolean Is_ini = myPrefs.getBoolean(ini, false);
        if (Is_ini==false){
            initDb();
            SharedPreferences.Editor editor = myPrefs.edit();
            editor.putBoolean(ini,true);
            editor.commit();
        }
        this.sqliteopenHelper = W_SqliteopenHelper.getInstance(context);
    }

    public void init(){
        sqLiteDatabase = sqliteopenHelper.getReadableDatabase();
    }

    public void close(){
        if (sqLiteDatabase.isOpen()==true) sqLiteDatabase.close();
    }

    public ArrayList<Word> find_by_prefix(String string){
        if (sqLiteDatabase.isOpen()!=true) {
            Log.d("DbManager","Dbmanager not init()");
            return null;
        }

        Cursor cursor =sqLiteDatabase.rawQuery("select * from "+tablename+" where spell like ? "+"order by id limit 200",new String[]{string+"%"});
        ArrayList<Word> arrayList = new ArrayList<Word>();
        while(cursor.moveToNext()) {
            arrayList.add(Word.Cursor2Word(cursor));
        }
        cursor.close();
        return arrayList;
    }

    public Word find_by_Id(int id){//id从1开始
        if (sqLiteDatabase.isOpen()!=true) {
            Log.d("DbManager","Dbmanager not init()");
            return null;
        }

        Cursor cursor = sqLiteDatabase.rawQuery("select * from "+tablename+" where id = ?",new String[]{""+id});
        //Cursor cursor = sqLiteDatabase.rawQuery("select * from "+tablename,null);
        Word word;
        if (cursor.moveToFirst()) {
            word = Word.Cursor2Word(cursor);
        }else {
            word = null;
        }
        cursor.close();
        return word;
    }

    private int Is_exist(String string){//0 --not found eles return id
        int index = 0;
        if (sqLiteDatabase.isOpen()!=true) {
            Log.d("DbManager","Dbmanager not init()");
            return 0;
        }

        Cursor cursor =sqLiteDatabase.rawQuery("select * from "+tablename+" where spell = ? ",new String[]{string});
        if (cursor.moveToFirst()) {
            int i = cursor.getColumnIndex("id");
            index = cursor.getInt(i);
        }
        cursor.close();
        return index;
    }

    public void Save_index(String string,int num){//num--用户选择的单词本号  string -- 要存入的单词拼写  此时dbmanager必须已经init
        int index = Is_exist(string);

        ContentValues contentValues = new ContentValues();

        if (index!=0) {
            user_sqliteopenHelper = User_SqliteopenHelper.getInstance(context,num);
            user_sqlitedatabase = user_sqliteopenHelper.getReadableDatabase();
            Cursor cursor = user_sqlitedatabase.rawQuery("select * from dic where dic_id = ?",new String[]{index+""});
            if (cursor.moveToFirst()) {
                cursor.close();
                user_sqlitedatabase.close();
                return;
            }
            cursor.close();
            contentValues.put("dic_id",index);
            contentValues.put("level",level_default);
        }else{
            return;
        }
        user_sqlitedatabase.insert("dic",null,contentValues);
        user_sqlitedatabase.close();
    }

    public ArrayList<Word> Note_show(int num){//num--用户选择的单词本号   此时dbmanager必须已经init

        int length = 0;

        if (sqLiteDatabase.isOpen()!=true) {
            Log.d("DbManager","Dbmanager not init()");
            return null;
        }

        user_sqliteopenHelper = User_SqliteopenHelper.getInstance(context,num);
        user_sqlitedatabase = user_sqliteopenHelper.getReadableDatabase();
        Cursor cursor = user_sqlitedatabase.rawQuery("select * from dic",null);
        Cursor cursor1;
        length = cursor.getCount();
        String id = "0" ;
        int level = 0 ;
        ArrayList<Word> arrayList = new ArrayList<Word>();
        for (int i =0; i<cursor.getCount()&&cursor.moveToNext();i++){
            id =""+ cursor.getInt(cursor.getColumnIndex("dic_id"));
            level = cursor.getInt(cursor.getColumnIndex("level"));
            cursor1 =sqLiteDatabase.rawQuery("select * from "+tablename+" where id = ? "+"",new String[]{id});
            if (cursor1.moveToFirst()){
                Word word = Word.Cursor2Word(cursor1);
                word.setLevel(level);
                arrayList.add(word);
            }
            cursor1.close();
        }
        cursor.close();
        user_sqlitedatabase.close();
        return arrayList;
    }

    private void initDb(){
        boolean k=((new File(DB_PATH + name)).exists() == false);
        File tmp=new File(DB_PATH + name);
        if ((new File(DB_PATH + name)).exists() == false) {
            //如 SQLite 数据库文件不存在，再检查一下 database 目录是否存在
            File f = new File(DB_PATH);
            // 如 database 目录不存在，新建该目录
            if (!f.exists()) {
                f.mkdir();
            }
        }else{
            tmp.delete();
        }
        try {
            // 得到 assets 目录下我们实现准备好的 SQLite 数据库作为输入流
            //InputStream is = context.getAssets().open("/"+name);
            InputStream is = context.getResources().openRawResource(
                    R.raw.gre3000);

            // 输出流
            OutputStream os = new FileOutputStream(DB_PATH + name);

            // 文件写入
            byte[] buffer = new byte[20000];
            int length;
            while ((length = is.read(buffer))!=-1) {
                os.write(buffer, 0, length);
            }

            // 关闭文件流
            os.flush();
            os.close();
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
