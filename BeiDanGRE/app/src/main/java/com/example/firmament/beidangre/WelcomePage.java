package com.example.firmament.beidangre;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.Toast;

/**欢迎界面
 * 加载数据与连接网络完成后
 * 自动跳转至主菜单
 */

public class WelcomePage extends Activity {

    private DbManager dbManager;

    public static SharedPreferences sharedPreferences;

    private static final int GOTO = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_page);

        dbManager = new DbManager(getApplicationContext());
        dbManager.init();

        sharedPreferences = getSharedPreferences("User_Setting",MODE_PRIVATE);
        int if_First = sharedPreferences.getInt("if_First",1);
        Boolean ifhave_notebook = sharedPreferences.getBoolean("ifhave_notebook",false);

        if (if_First == 1){
            //All_Lists.SETGRELIST();
            for (Integer i = 0; i < All_Lists.NumOfGREList; i ++){
                VocabularyList vocabularyList = new VocabularyList("List " + i.toString(),50);
                for (Integer j = 0; j < 50; j ++) {
                    Vocabulary vocabulary = new Vocabulary();

                    String[] strings = new String[3];
                    strings[0] = dbManager.find_by_Id(j + 1).getExp1();
                    strings[1] = dbManager.find_by_Id(j + 1).getExp2();
                    strings[2] = dbManager.find_by_Id(j + 1).getExp3();

                    vocabulary.setWord(dbManager.find_by_Id(j + 1).getName());
                    vocabulary.setMeanings(strings);
                    vocabularyList.AddVocabulary(vocabulary);
                }
                All_Lists.GRELists[i] = vocabularyList;
            }

            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt("if_First",0);
            editor.commit();
        }else {
            //All_Lists.SETGRELIST();
            for (Integer i = 0; i < All_Lists.NumOfGREList; i ++){
                VocabularyList vocabularyList = new VocabularyList("List " + i.toString(),50);
                for (Integer j = 0; j < 50; j ++) {
                    Vocabulary vocabulary = new Vocabulary();

                    String[] strings = new String[3];
                    strings[0] = dbManager.find_by_Id(j + 1).getExp1();
                    strings[1] = dbManager.find_by_Id(j + 1).getExp2();
                    strings[2] = dbManager.find_by_Id(j + 1).getExp3();

                    vocabulary.setWord(dbManager.find_by_Id(j + 1).getName());
                    vocabulary.setVoice("");
                    vocabulary.setMeanings(strings);
                    vocabularyList.AddVocabulary(vocabulary);
                }
                All_Lists.GRELists[i] = vocabularyList;
            }
        }

        if (ifhave_notebook == true){
            All_Lists.SETNOTEBOOK();
        }else {
            ;
        }

        dbManager.close();

        MyTimer timer = new MyTimer();
        timer.start();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.welcome_page, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    Handler mHandler = new Handler() {
        public void handleMessage(Message msg){
            switch (msg.what){
                case GOTO:
                    Intent intent1 = new Intent(WelcomePage.this,MainMenu.class);
                    startActivity(intent1);
                    finish();
                    break;
                default:
                    break;
            }
        }
    };

    private class MyTimer extends Thread{

        public MyTimer(){
            ;
        }

        @Override
        public void run(){
            try {
                Thread.sleep(1000);
                mHandler.sendEmptyMessage(GOTO);
            }
            catch (InterruptedException e){
                e.printStackTrace();
            }
        }
    }
}
