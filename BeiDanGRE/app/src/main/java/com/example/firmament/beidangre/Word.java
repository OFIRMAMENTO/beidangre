package com.example.firmament.beidangre;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Firmament on 14-11-4.
 * 从数据库中读取单词的词汇接口类
 */

public class Word implements Parcelable {

    private String name = null;

    private String exp1 = null;

    private String exp2 = null;

    private String exp3 = null;

    private int id = 0;

    private int level = 0;

    public Word(String name,String exp1,String exp2,String exp3,int id){

        this.name = name;
        this.exp1 = exp1;
        this.exp2 = exp2;
        this.exp3 = exp3;
        this.id = id;
        this.level = 0;
    }

    public String getExp1() {
        return exp1;
    }

    public String getExp2() {
        return exp2;
    }

    public String getExp3() {
        return exp3;
    }

    public String getName(){
        return name;
    }

    public int getId(){
        return id;
    }

    public int getLevel() { return level;}

    public void setName(String name) {
        this.name = name;
    }

    public void setExp1(String exp1) {
        this.exp1 = exp1;
    }

    public void setExp2(String exp2) {
        this.exp2 = exp2;
    }

    public void setExp3(String exp3) {
        this.exp3 = exp3;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLevel(int level){ this.level = level;}


    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags)
    {
        out.writeString(name);
        out.writeString(exp1);
        out.writeString(exp2);
        out.writeString(exp3);
        out.writeInt(id);
        out.writeInt(level);
    }

    public static final Parcelable.Creator<Word> CREATOR = new Creator<Word>()
    {
        @Override
        public Word[] newArray(int size)
        {
            return new Word[size];
        }

        @Override
        public Word createFromParcel(Parcel in)
        {
            return new Word(in);
        }
    };

    private Word(Parcel in)
    {
        name = in.readString();
        exp1 = in.readString();
        exp2 = in.readString();
        exp3 = in.readString();
        id = in.readInt();
        level = in.readInt();
    }

    public static Word Cursor2Word(Cursor cursor){
        if (cursor== null||cursor.isClosed()) {
            return null;
        }
        int int_spell = cursor.getColumnIndex("spell");
        String spell  = cursor.getString(int_spell);

        int int_id = cursor.getColumnIndex("id");

        int id = cursor.getInt(int_id);

        int int_exp1 = cursor.getColumnIndex("point1");
        String exp1 = cursor.getString(int_exp1);

        int int_exp2 = cursor.getColumnIndex("point2");
        String exp2 = cursor.getString(int_exp2);

        int int_exp3 = cursor.getColumnIndex("point3");
        String exp3 = cursor.getString(int_exp3);

        return new Word(spell,exp1,exp2,exp3,id);

    }
}
