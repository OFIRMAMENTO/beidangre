package com.example.firmament.beidangre;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;


public class GREWords extends Activity {

    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gre_words);

        final Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        final int ListNo = bundle.getInt("ListNo");

        listView = (ListView)findViewById(R.id.gre_word_listView);
        GREWordsAdapter greWordsAdapter = new GREWordsAdapter(this,ListNo);
        listView.setAdapter(greWordsAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                bundle.putInt("ListNo",ListNo);
                bundle.putInt("WordNo",position);
                bundle.putSerializable("Vocabulary",All_Lists.GRELists[ListNo].vocabularies[position]);

                Intent intent1 = new Intent(GREWords.this,GWords.class);
                intent1.putExtras(bundle);
                startActivity(intent1);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_grewords, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
