package com.example.firmament.beidangre;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

/**
 * Created by Firmament on 14-10-30.
 */
public class NoteBookWordsFragment extends Fragment {

    private FragmentManager fragmentManager;

    private ListView listView;

    private NWordsFragment nWordsFragment;

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {
        View Layout = inflater.inflate(R.layout.note_book_words_fragment, container, false);

        fragmentManager = getFragmentManager();

        final int ListNo = getArguments().getInt("ListNo");

        listView = (ListView)Layout.findViewById(R.id.note_book_words_listView);
        NoteBookWordsAdapter noteBookWordsAdapter = new NoteBookWordsAdapter(getActivity(),ListNo);
        listView.setAdapter(noteBookWordsAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                bundle.putInt("ListNo",ListNo);
                bundle.putInt("WordNo",position);
                bundle.putSerializable("Vocabulary",All_Lists.NoteBooks[ListNo].vocabularies[position]);

                nWordsFragment = NWordsFragment.getInstance(bundle);
                fragmentManager.beginTransaction().addToBackStack(null).replace(R.id.fragment_container0,nWordsFragment).commit();
            }
        });

        return Layout;
    }

    public static NoteBookWordsFragment getInstance(Bundle bundle){
        NoteBookWordsFragment noteBookWordsFragment = new NoteBookWordsFragment();
        noteBookWordsFragment.setArguments(bundle);
        return noteBookWordsFragment;
    }
}
