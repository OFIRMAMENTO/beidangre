package com.example.firmament.beidangre;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TabHost;
import android.widget.Toast;
import android.widget.ViewFlipper;


public class MainMenu extends Activity {

    private TabHost tabHost;

    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;

    public static ResideMenu resideMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        tabHost = (TabHost)findViewById(R.id.main_menu_tabHost);
        tabHost.setup();

        fragmentManager = getFragmentManager();

        NoteBookFragment noteBookFragment = new NoteBookFragment();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragment_container0,noteBookFragment).commit();

        tabHost.addTab(tabHost.newTabSpec("NoteBook").setIndicator("",getResources().getDrawable(R.drawable.ic_launcher)).setContent(R.id.fragment_container0));
        tabHost.addTab(tabHost.newTabSpec("Dictionary").setIndicator("",getResources().getDrawable(R.drawable.ic_launcher)).setContent(R.id.fragment_container1));
        tabHost.addTab(tabHost.newTabSpec("Community").setIndicator("",getResources().getDrawable(R.drawable.ic_launcher)).setContent(R.id.fragment_container2));

        //attach to current activity;
        resideMenu = new ResideMenu(this);
        //resideMenu.setBackground(R.drawable.ic_launcher);
        resideMenu.attachToActivity(this);
        resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_RIGHT);

        // create menu items;
        String titles[] = { "主页", "复习计划", "GRE宝典", "设置" };
        int icon[] = { R.drawable.ic_launcher, R.drawable.ic_launcher, R.drawable.ic_launcher, R.drawable.ic_launcher };

        for (int i = 0; i < titles.length; i++){
            ResideMenuItem item = new ResideMenuItem(this, icon[i], titles[i]);
            switch (i) {
                case 0:
                    item.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            resideMenu.closeMenu();
                        }
                    });
                    break;
                case 1:
                    item.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(MainMenu.this, "bbb", Toast.LENGTH_SHORT).show();
                        }
                    });
                    break;
                case 2:
                    item.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent2 = new Intent(MainMenu.this,GREList.class);
                            startActivity(intent2);
                        }
                    });
                    break;
                case 3:
                    item.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(MainMenu.this, "ddd", Toast.LENGTH_SHORT).show();
                        }
                    });
                    break;
                default:
                    break;
            }
            resideMenu.addMenuItem(item,  ResideMenu.DIRECTION_LEFT); // or  ResideMenu.DIRECTION_RIGHT
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return resideMenu.dispatchTouchEvent(ev);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
