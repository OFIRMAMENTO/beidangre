package com.example.firmament.beidangre;

import java.io.Serializable;

/**
 * Created by Firmament on 14-10-30.
 *一个词汇的基本类；
 * 用户不能创建一个单词，只能修改其中的level属性；
 * Vocabulary的使用方法，在外部新建一个Vocabulary，通过set写入，然后复制给List中静态的Vocabulary；
 * 显示时，调用get读出数据；
 */

public class Vocabulary implements Serializable {

    private String word;  //词汇的拼写；

    private String voice;     //词汇的音标

    private String[] meanings;    //词汇的意思，意思不只有一个，词性随意思一同显示；

    private Integer level;  //标记词汇的重点程度；

    //构造函数；
    public Vocabulary(){
        word = "";
        voice = "";
        meanings = new String[3];
        for (int i = 0; i < meanings.length; i++){
            meanings[i] = "";
        }
        level = 1;
    }

    //写入数据方法；
    public void setWord(String words){
        word = words;
    }

    public void setVoice(String voices){
        voice = voices;
    }

    public void setMeanings(String[] meaning){
        meanings = meaning;
    }

    public void setLevel(Integer levels){
        level = levels;
    }

    //读取数据方法；
    public String getWord(){
        return word;
    }

    public String getVoice(){
        return voice;
    }

    public String[] getMeanings(){
        return meanings;
    }

    public Integer getLevel(){
        return level;
    }
}
