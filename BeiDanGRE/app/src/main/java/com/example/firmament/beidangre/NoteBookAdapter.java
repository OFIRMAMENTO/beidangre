package com.example.firmament.beidangre;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Firmament on 14-10-30.
 */
public class NoteBookAdapter extends BaseAdapter{

    private LayoutInflater inflater;

    private List<Map<String,Object>> Data;

    public NoteBookAdapter(Context context){
        inflater = LayoutInflater.from(context);
        init();
    }

    private void init(){
        Data = new ArrayList<Map<String, Object>>();
        for (int i = 0; i < All_Lists.NumOfNoteBook; i++){
            Map<String,Object> map = new HashMap<String, Object>();
            map.put("image",R.drawable.ic_launcher);
            map.put("listNo",All_Lists.NoteBooks[i].Name);
            Data.add(map);
        }
    }

    public int getCount(){
        return Data.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position,View convertView,ViewGroup parent){
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.note_book_listitems, null);
            holder.image = (ImageView)convertView.findViewById(R.id.note_book_listView_image);
            holder.text = (TextView)convertView.findViewById(R.id.note_book_listView_text);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }
        holder.image.setBackgroundResource((Integer)Data.get(position).get("image"));
        holder.text.setText(Data.get(position).get("listNo").toString());
        return convertView;
    }

    public final class ViewHolder {
        public ImageView image;
        public TextView text;
    }
}
