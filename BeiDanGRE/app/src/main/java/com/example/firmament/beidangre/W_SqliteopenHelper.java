package com.example.firmament.beidangre;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Firmament on 14-11-4.
 * 数据库打开类；
 */

public class W_SqliteopenHelper extends SQLiteOpenHelper {

    final static private int version = 1;

    final private static String name = "gre3000.db";

    final private static SQLiteDatabase.CursorFactory factory = null;

    private static Context context = null;

    private volatile static W_SqliteopenHelper instance = null;

    public static W_SqliteopenHelper getInstance(Context context) {

        if (instance == null) {
            synchronized (W_SqliteopenHelper.class) {
                if(instance == null) {
                    instance = new W_SqliteopenHelper(context);
                }
            }
        }
        return instance;
    }

    private W_SqliteopenHelper (Context context){
        super(context,name,factory,version);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase database){//具体还得改啊
        ;
    }

    @Override
    public void onUpgrade(SQLiteDatabase database,int oldVersion,int newVesion){

    }
}
