package com.example.firmament.beidangre;

/**
 * Created by Firmament on 14-10-30.
 * 一个单词列表，里面包含很多Vocabulary；从而对词汇进行分类；
 * GREList与NoteBook均由这个类创建；
 * GREList在程序开始时已经默认生成好，只有单词本才允许用户动态创建；
 */

public class VocabularyList {

    public String Name; //词汇表的名称；

    public Integer Numbers = 0; //词汇表包含的词汇数量；

    public int precessing = 0;  //表示单词本的复习进度;

    public Vocabulary[] vocabularies;   //词汇的Vocabulary数组；

    //构造函数,创建一个空的单词本；
    public VocabularyList(String name,int Num){
        vocabularies = new Vocabulary[Num];    //一个词汇表的容量最多为3000个；
        Name = name;
        Numbers = 0;
        precessing = 0;
    }

    //添加一个词汇；
    public void AddVocabulary(Vocabulary vocabulary){
        vocabularies[Numbers] = vocabulary;
        Numbers ++;
    }

    //删除一个词汇；
    public void DeleteVocabulary(int No){   //根据序号删除；
        for (int i = No + 1; i < Numbers; i++){
            vocabularies[i - 1] = vocabularies[i];
        }
        Numbers --;
    }
}
