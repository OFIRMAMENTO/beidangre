package com.example.firmament.beidangre;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class GWords extends Activity {

    private int ListNo;
    private int WordNo;

    private Vocabulary vocabulary;

    private TextView word;
    private TextView voice;
    private TextView meanings;

    private Button previous;
    private Button next;

    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gwords);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        ListNo = bundle.getInt("ListNo");
        WordNo = bundle.getInt("WordNo");
        vocabulary = (Vocabulary)bundle.getSerializable("Vocabulary");

        position = WordNo;

        word = (TextView)findViewById(R.id.vocabulary_word);
        voice = (TextView)findViewById(R.id.vocabulary_voice);
        meanings = (TextView)findViewById(R.id.vocabulary_meanings);

        previous = (Button)findViewById(R.id.vocabulary_previous);
        next = (Button)findViewById(R.id.vocabulary_next);

        ShowData(vocabulary);

        previous.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (position > 0) {
                    position--;
                    Vocabulary vocabulary1 = All_Lists.GRELists[ListNo].vocabularies[position];
                    ShowData(vocabulary1);

                    if (position <= 0){
                        previous.setClickable(false);
                        next.setClickable(true);
                    }else if (position < All_Lists.GRELists[ListNo].Numbers - 1){
                        previous.setClickable(true);
                        next.setClickable(true);
                    }else {
                        previous.setClickable(true);
                        next.setClickable(false);
                    }
                }
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position < All_Lists.GRELists[ListNo].Numbers - 1) {
                    next.setClickable(true);
                    position++;
                    Vocabulary vocabulary2 = All_Lists.GRELists[ListNo].vocabularies[position];
                    ShowData(vocabulary2);

                    if (position <= 0){
                        previous.setClickable(false);
                        next.setClickable(true);
                    }else if (position < All_Lists.GRELists[ListNo].Numbers - 1){
                        previous.setClickable(true);
                        next.setClickable(true);
                    }else {
                        previous.setClickable(true);
                        next.setClickable(false);
                    }
                }
            }
        });
    }

    private void ShowData(Vocabulary vo){
        word.setText(vo.getWord());
        voice.setText(vo.getVoice());
        String meaning = new String("");
        for (int i = 0; i < 3; i ++){
            meaning = meaning + "\n" + vo.getMeanings()[i];
        }
        meanings.setText(meaning);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_gwords, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
