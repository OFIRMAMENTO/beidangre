package com.example.firmament.beidangre;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Firmament on 14-10-30.
 */
public class NWordsFragment extends Fragment {

    private FragmentManager fragmentManager;

    private TextView word;
    private TextView voice;
    private TextView meanings;

    private Button previous;
    private Button next;

    private int position;

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {
        View Layout = inflater.inflate(R.layout.nwords_fragment, container, false);
        final int ListNo = getArguments().getInt("ListNo");
        final int WordNo = getArguments().getInt("WordNo");
        Vocabulary vocabulary = (Vocabulary) getArguments().getSerializable("Vocabulary");

        position = WordNo;

        word = (TextView)Layout.findViewById(R.id.vocabulary_word);
        voice = (TextView)Layout.findViewById(R.id.vocabulary_voice);
        meanings = (TextView)Layout.findViewById(R.id.vocabulary_meanings);

        previous = (Button)Layout.findViewById(R.id.vocabulary_previous);
        next = (Button)Layout.findViewById(R.id.vocabulary_next);

        ShowData(vocabulary);

        previous.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (position > 0) {
                    position--;
                    Vocabulary vocabulary1 = All_Lists.NoteBooks[ListNo].vocabularies[position];
                    ShowData(vocabulary1);

                    if (position <= 0){
                        previous.setClickable(false);
                        next.setClickable(true);
                    }else if (position < All_Lists.NoteBooks[ListNo].Numbers - 1){
                        previous.setClickable(true);
                        next.setClickable(true);
                    }else {
                        previous.setClickable(true);
                        next.setClickable(false);
                    }
                }
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position < All_Lists.NoteBooks[ListNo].Numbers - 1) {
                    next.setClickable(true);
                    position++;
                    Vocabulary vocabulary2 = All_Lists.NoteBooks[ListNo].vocabularies[position];
                    ShowData(vocabulary2);

                    if (position <= 0){
                        previous.setClickable(false);
                        next.setClickable(true);
                    }else if (position < All_Lists.NoteBooks[ListNo].Numbers - 1){
                        previous.setClickable(true);
                        next.setClickable(true);
                    }else {
                        previous.setClickable(true);
                        next.setClickable(false);
                    }
                }
            }
        });

        return Layout;
    }

    private void ShowData(Vocabulary vo) {
        word.setText(vo.getWord());
        voice.setText(vo.getVoice());
        String meaning = new String("");
        for (int i = 0; i < 10; i++) {
            meaning = meaning + "\n" + vo.getMeanings()[i];
        }
        meanings.setText(meaning);
    }

    public static NWordsFragment getInstance(Bundle bundle){
        NWordsFragment nWordsFragment = new NWordsFragment();
        nWordsFragment.setArguments(bundle);
        return nWordsFragment;
    }
}
