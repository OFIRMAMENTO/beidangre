package com.example.firmament.beidangre;

/**
 * Created by Firmament on 14-10-30.
 * 这个类用来管理所有单词列表
 * 包括静态的GREList，自动加载；
 * 包括静态的NoteBook，可有用户创建；
 * 生成一个单词本是一个静态方法，再此类中调用；
 */

public class All_Lists {

    public static VocabularyList[] GRELists = new VocabularyList[100];

    public static VocabularyList[] NoteBooks = new VocabularyList[100];

    public static int NumOfGREList = 30; //GREList的个数；

    public static int NumOfNoteBook = 0;    //NoteBook的个数；

    public All_Lists(){
        ;
    }

    //数据初始化;
    public static void SETGRELIST(){
        for (Integer i = 0; i < NumOfGREList; i ++){
            VocabularyList vocabularyList = new VocabularyList("List " + i.toString(),100);
            for (Integer j = 0; j < 100; j ++) {
                Vocabulary vocabulary = new Vocabulary();
                vocabulary.setWord(i.toString() + j.toString());

                vocabularyList.AddVocabulary(vocabulary);
            }
            GRELists[i] = vocabularyList;
        }
    }

    public static void SETNOTEBOOK(){
        for (Integer i = 0; i < NumOfNoteBook; i ++){
            VocabularyList vocabularyList = new VocabularyList("List" + i.toString(),1000);
            for (Integer j = 0; j < 30; j ++){
                Vocabulary vocabulary = new Vocabulary();
                vocabulary.setWord(i.toString() + j.toString());

                vocabularyList.AddVocabulary(vocabulary);
            }
            NoteBooks[i] = vocabularyList;
        }
    }

    //创建一个静态的NoteBook词汇列表；
    public static void CreateList(String name){
        VocabularyList vocabularyList = new VocabularyList(name,1000);
        NoteBooks[NumOfNoteBook] = vocabularyList;
        NumOfNoteBook ++;
    }

}
