package com.example.firmament.beidangre;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Firmament on 14-10-30.
 */
public class NoteBookWordsAdapter extends BaseAdapter {

    private LayoutInflater inflater;

    private ArrayList<HashMap<String,Object>> Data;

    public NoteBookWordsAdapter(Context context,int position){
        inflater = LayoutInflater.from(context);
        init(position);
    }

    private void init(int position){
        Data = new ArrayList<HashMap<String, Object>>();
        for (int i = 0; i < All_Lists.NoteBooks[position].Numbers && position < All_Lists.NumOfNoteBook; i++) {
            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("words", All_Lists.NoteBooks[position].vocabularies[i].getWord());
            Data.add(map);
        }
    }

    public int getCount(){
        return Data.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position,View convertView,ViewGroup parent){
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.note_book_words_listitems, null);
            holder.text = (TextView)convertView.findViewById(R.id.note_book_words_listView_text);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }
        holder.text.setText(Data.get(position).get("words").toString());
        return convertView;
    }

    public final class ViewHolder {
        public TextView text;
    }
}
